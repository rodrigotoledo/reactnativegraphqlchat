import React from 'react';

import { Platform, Dimensions, StatusBar, View, ScrollView, Text, StyleSheet } from 'react-native';

const Chat = () => (
  <View style={styles.container}>
    <ScrollView contentContainerStyle={styles.conversation}>
      <View style={styles.bubble}>
        <Text style={styles.author}>Diego</Text>
        <Text style={styles.message}>Olá galera</Text>
      </View>
    </ScrollView>
  </View>
);

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2C4241',
    ...Platform.select({
      ios: {
        paddingTop: 20,
      }
    })
  },
  conversation: {
    padding: 10,
  },
  bubble: {
    padding: 6,
    backgroundColor: '#f5f5f5',
    borderRadius: 6,
    shadowColor: 'rgba(0,0,0,0.5)',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.5,
    shadowRadius: 0,
    marginTop: 10,
    maxWidth: width - 60,
  },
  author: {
    fontWeight: 'bold',
    marginBottom: 3,
    color: '#333',
  },
  message: {
    fontSize: 16,
    color: '#333',
  }
});
  
export default Chat;